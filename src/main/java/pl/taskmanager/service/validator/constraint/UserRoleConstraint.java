package pl.taskmanager.service.validator.constraint;

import pl.taskmanager.service.validator.constraintvalidator.RoleUserConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=RoleUserConstraintValidator.class)
public @interface UserRoleConstraint {

    String message() default "This role doesn't not exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface List {
        UserRoleConstraint[] value();
    }
}
