package pl.taskmanager.service.validator.constraint;


import pl.taskmanager.service.validator.constraintvalidator.ProjectStatusConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=ProjectStatusConstraintValidator.class)
public @interface ProjectStatusConstraint {

    String message() default "This status doesn't not exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    public @interface List {
        ProjectStatusConstraint[] value();
    }
}
