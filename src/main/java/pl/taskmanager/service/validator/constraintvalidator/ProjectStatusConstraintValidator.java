package pl.taskmanager.service.validator.constraintvalidator;

import pl.taskmanager.entity.ProjectStatus;
import pl.taskmanager.service.validator.constraint.ProjectStatusConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ProjectStatusConstraintValidator implements ConstraintValidator<ProjectStatusConstraint, ProjectStatus> {

    public ProjectStatusConstraintValidator()
    {}

    @Override
    public void initialize(ProjectStatusConstraint productStatusConstraint)
    {

    }

    @Override
    public boolean isValid(ProjectStatus status, ConstraintValidatorContext constraintValidatorContext)
    {
        if (ProjectStatus.contains(status.toString())) {
            return true;
        } else {
            return false;
        }

    }
}
