package pl.taskmanager.service.validator.constraintvalidator;

import pl.taskmanager.entity.UserRole;
import pl.taskmanager.entity.UserRoleType;
import pl.taskmanager.service.validator.constraint.UserRoleConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

public class RoleUserConstraintValidator implements ConstraintValidator<UserRoleConstraint, Set<UserRole>> {

    public RoleUserConstraintValidator()
    {}

    @Override
    public void initialize(UserRoleConstraint userRoleConstraint)
    {

    }

    @Override
    public boolean isValid(Set<UserRole> userRoles, ConstraintValidatorContext constraintValidatorContext)
    {
        if (userRoles.isEmpty()) {
            return false;
        }

        UserRole userRole = userRoles.iterator().next();

        if (userRole != null && UserRoleType.contains(userRole.getName().name())) {
            return true;
        } else {
            return false;
        }
    }
}
