package pl.taskmanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pl.taskmanager.entity.AuthorizedUser;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.findOneByUsername(username);

		if (user == null || user.getUsername().equals(username) == false) {
			throw new UsernameNotFoundException("Username not found");
		}

		return new AuthorizedUser(user);
	}

}
