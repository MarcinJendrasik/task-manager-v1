package pl.taskmanager.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.service.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
		auth.userDetailsService(userDetailsService);
		auth.authenticationProvider(authenticationProvider());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.csrf().disable();
		http.authorizeRequests()
			.antMatchers("/backlog/project/{project_id}").access("@webSecurityAccessBacklog.check(authentication,#project_id)")
			.antMatchers("/project/{project_id}/").access("@webSecurityAccessBacklog.check(authentication,#project_id)")
			.antMatchers("/project/{project_id}/sprint/delete/{sprint_id}").access("@webSecurityAccessSprint.check(authentication, #project_id, #sprint_id)")
			.antMatchers("/project/{project_id}/task/delete/{task_id}").access("@webSecurityAccessTask.check(authentication, #project_id, #task_id)")
			.antMatchers("/task/{task_id}/comment/delete/{task_comment_id}").access("@webSecurityAccessTaskComment.check(authentication, #task_id, #task_comment_id)")
			.antMatchers("/task/{task_id}/comment/update/{task_comment_id}").access("@webSecurityAccessTaskComment.check(authentication, #task_id, #task_comment_id)")
			.antMatchers("/secured/user/update/status/{user_id}").access("@webSecurityAccessUser.check(authentication, #user_id)")
			.antMatchers("/secured/user/update/{user_id}").access("@webSecurityAccessUser.check(authentication, #user_id)")
			.antMatchers("**/secured/**").authenticated()
			.anyRequest().permitAll()
				.and()
			.formLogin()
				.loginPage("/login")
				.failureUrl("/login-error")
				.permitAll()
				.defaultSuccessUrl("/project/");
	}
}
