package pl.taskmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.taskmanager.config.WebSecurity;
import pl.taskmanager.entity.AuthorizedUser;
import pl.taskmanager.entity.Task;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.TaskRepository;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.service.UserService;

@Component
public class WebSecurityAccessTask extends WebSecurity {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserService userService;

    public boolean check(Authentication authentication, int projectId, int taskId)
    {
        if(isAnonymous(authentication))
            return false;

        AuthorizedUser customUserDetails = (AuthorizedUser)authentication.getPrincipal();

        User user = userRepository.findOneByUsername(customUserDetails.getUsername());

        if (user.hasRoleAdmin())
            return true;

        Task task = taskRepository.findOne(taskId);

        if (userService.isUserHasRoleDeveloperAndIsOwnerTask(user, task))
            return true;

        return false;
    }
}
