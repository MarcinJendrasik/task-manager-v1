package pl.taskmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.taskmanager.config.WebSecurity;
import pl.taskmanager.entity.AuthorizedUser;
import pl.taskmanager.entity.Project;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.UserRepository;

@Component
public class WebSecurityAccessBacklog extends WebSecurity {

    @Autowired
    UserRepository userRepository;

    public boolean check(Authentication authentication, int projectId)
    {
        if(isAnonymous(authentication)) {
            return false;
        }

        AuthorizedUser customUserDetails = (AuthorizedUser)authentication.getPrincipal();

        User user = userRepository.findOneByUsername(customUserDetails.getUsername());

        if (user.hasRoleAdmin())
            return true;

        for (Project project : user.getProjects()) {
            if (project.getId() == projectId) {
                return true;
            }
        }

        return false;
    }
}
