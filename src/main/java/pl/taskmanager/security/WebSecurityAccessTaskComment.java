package pl.taskmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.taskmanager.config.WebSecurity;
import pl.taskmanager.entity.AuthorizedUser;
import pl.taskmanager.entity.TaskComment;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.TaskCommentRepository;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.service.UserService;

@Component
public class WebSecurityAccessTaskComment extends WebSecurity {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskCommentRepository taskCommentRepository;

    @Autowired
    UserService userService;

    public boolean check(Authentication authentication, int taskId, int taskCommentId)
    {
        if(isAnonymous(authentication))
            return false;

        AuthorizedUser customUserDetails = (AuthorizedUser)authentication.getPrincipal();

        User user = userRepository.findOneByUsername(customUserDetails.getUsername());

        if (user.hasRoleAdmin())
            return true;

        TaskComment taskComment = taskCommentRepository.findOne(taskCommentId);

        if (userService.isUserHasRoleDeveloperAndIsOwnerTaskComment(user, taskComment))
            return true;

        return false;
    }
}
