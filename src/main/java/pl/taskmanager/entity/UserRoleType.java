package pl.taskmanager.entity;

public enum UserRoleType
{
	ROLE_ADMIN,
	ROLE_DEVELOPER,
	ROLE_NOT_LOGGED;

	public static boolean contains(String value) {

		for (UserRoleType role : UserRoleType.values()) {
			String fullRole = role.name();
			if (fullRole.equals(value)) {
				return true;
			}
		}

		return false;
	}
}