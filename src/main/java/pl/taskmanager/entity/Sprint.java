package pl.taskmanager.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "sprint")
public class Sprint {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotEmpty
	@Column(name = "name")
	private String name;

	@NotEmpty
	@Column(name = "goal")
	private String goal;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private SprintStatus status;

	@Column(name = "date_start")
	private Date dateStart;
	
	@Column(name = "date_stop")
	private Date dateStop;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false)
	private Project project;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "created_by", nullable = false)
	private User user;

//	@JsonIgnore
	@OrderBy("id ASC")
	@OneToMany(mappedBy = "sprint")
	private Set<Task> tasks;

	@Column(name = "created_at")
	private Date createdAt;

	public Sprint()
	{}

	public Sprint(int id)
	{
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGoal() {
		return goal;
	}

	public void setGoal(String goal) {
		this.goal = goal;
	}

	public SprintStatus getStatus() {
		return status;
	}

	public void setStatus(SprintStatus status) {
		this.status = status;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateStop() {
		return dateStop;
	}

	public void setDateStop(Date dateStop) {
		this.dateStop = dateStop;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
