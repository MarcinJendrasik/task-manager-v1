package pl.taskmanager.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user_role")
public class UserRole {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "name", unique = true)
	private UserRoleType name;

	@ManyToMany(mappedBy = "userRoles")
	private Set<User> users = new HashSet<User>();

	public int getId() {
		return id;
	}

	public UserRoleType getName() {
		return name;
	}

	public void setName(UserRoleType name) {
		this.name = name;
	}

	public boolean isRoleAdmin() {
		return this.getName() == UserRoleType.ROLE_ADMIN;
	}

	public boolean isRoleDeveloper() {
		return this.getName() == UserRoleType.ROLE_DEVELOPER;
	}
}
