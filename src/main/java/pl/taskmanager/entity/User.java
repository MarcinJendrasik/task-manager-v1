package pl.taskmanager.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.taskmanager.service.validator.constraint.UserRoleConstraint;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user")
public class User {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@NotBlank
	@Column(name = "username", unique=true)
	private String username;

	@NotBlank
	@Email
	@Column(name = "email", unique=true)
	private String email;

	@Column(name = "enable")
	private boolean enable;

	@NotBlank
	@Size(min = 8)
	@Column(name = "password")
	private String password;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_has_role",
             joinColumns = { @JoinColumn(name = "user_id") },
             inverseJoinColumns = { @JoinColumn(name = "user_role_id") })
	@NotEmpty
	@UserRoleConstraint
	@Valid
	private Set<UserRole> userRoles = new HashSet<UserRole>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@OrderBy("id ASC")
	@JoinTable(name = "project_participant",
			joinColumns = { @JoinColumn(name = "user_id") },
			inverseJoinColumns = { @JoinColumn(name = "project_id") })
	private Set<Project> projects = new HashSet<Project>();

	@OneToMany(mappedBy = "user")
	private Set<Sprint> sprints;

	@OneToMany(mappedBy = "createdBy")
	private Set<Task> tasks;
	
	@OneToMany(mappedBy = "user")
	private Set<TaskComment> taskComments;

	@OneToMany(mappedBy = "assignee")
	private Set<Task> assigneeToTasks;
	
	public User() {
		
	}

	public User(int id)
	{
		this.id = id;
	}
	
	public User(User user) {
		username = user.getUsername();
		email = user.getEmail();
		password = user.getPassword();
		enable = user.isEnable();
		userRoles = user.getUserRoles();
		projects = user.getProjects();
		sprints = user.getSprints();
		tasks = user.getTasks();
		taskComments = getTaskComments();
	}

	public int getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
    public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	public Set<Sprint> getSprints() {
		return sprints;
	}

	public void setSprints(Set<Sprint> sprints) {
		this.sprints = sprints;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public Set<TaskComment> getTaskComments() {
		return taskComments;
	}

	public void setTaskComments(Set<TaskComment> taskComments) {
		this.taskComments = taskComments;
	}

	public Set<Task> getAssigneeToTasks() {
		return assigneeToTasks;
	}

	public void setAssigneeToTasks(Set<Task> assigneeToTasks) {
		this.assigneeToTasks = assigneeToTasks;
	}

	public boolean hasRoleAdmin() {
		for (UserRole userRole : this.getUserRoles()) {

			if (userRole.isRoleAdmin()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		//id
		if (id != other.getId()) return false;
		//username
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		//email
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		//enable
		if (enable != other.enable) return false;
		//password
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;

		return true;
	}
}
