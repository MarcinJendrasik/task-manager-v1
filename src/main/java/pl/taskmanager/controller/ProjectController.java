package pl.taskmanager.controller;

import java.security.Principal;
import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.taskmanager.entity.Project;
import pl.taskmanager.entity.ProjectStatus;
import pl.taskmanager.entity.User;
import pl.taskmanager.entity.UsersParticipants;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.UserRepository;

@RestController
@RequestMapping("/project")
@PreAuthorize("hasAnyRole('ADMIN','DEVELOPER')")
public class ProjectController {
	
	private ProjectRepository projectRepository;
	private UserRepository userRepository;

	@Autowired
	ProjectController(ProjectRepository projectRepository,
					  UserRepository userRepository)
	{
		this.projectRepository = projectRepository;
		this.userRepository = userRepository;
	}

	@GetMapping("/")
	public ModelAndView get(@RequestParam(defaultValue = "0") int page,
							Principal principal)
	{
		String name = principal.getName();
		User user = userRepository.findOneByUsername(name);

		ModelAndView modelAndView = new ModelAndView();
		if (user.hasRoleAdmin())
		{
			modelAndView.addObject("projects", projectRepository.findAll());
		} else {
			modelAndView.addObject("projects", user.getProjects());
		}
		
		modelAndView.addObject("currentPage", page);

		modelAndView.addObject("view", "project/project_list");
		modelAndView.setViewName("base");
		return modelAndView;
	}
	
	@GetMapping("/add")
	public ModelAndView add()
	{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("base");
		modelAndView.addObject("view", "project/project_add");

		modelAndView.addObject("project", new Project());
		modelAndView.addObject("statuses", ProjectStatus.values());
		return modelAndView;
	}
	
	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView add(@Valid @ModelAttribute Project project,
						    BindingResult bindingResult,
						    Principal principal,
							RedirectAttributes redir)
	{
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("base");
			modelAndView.addObject("statuses", ProjectStatus.values());
			modelAndView.addObject("view", "project/project_add");
			modelAndView.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
			return modelAndView;
		}

		String name = principal.getName();
		User user = userRepository.findOneByUsername(name);
		project.setCreatedBy(user);

		Set<User> users = new HashSet<>();
		users.add(user);

		project.setCreatedAt(new Date());
		project.setUsers(users);

		if (!bindingResult.hasErrors()) {
			projectRepository.save(project);
		}

		redir.addFlashAttribute("statement", "Success create Project");

		return new ModelAndView("redirect:/project/");
	}
	
	@GetMapping("/update/{id}")
	public ModelAndView edit(@PathVariable("id") int id)
	{
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("id", id);
		modelAndView.addObject("project", projectRepository.findOne(id));
		modelAndView.addObject("statuses", ProjectStatus.values());

		modelAndView.addObject("view", "project/project_edit");
		modelAndView.setViewName("base");

		return modelAndView;
	}
	
	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView update(@PathVariable("id") int id,
							   @Valid @ModelAttribute Project project,
							   BindingResult bindingResult,
							   RedirectAttributes redir)
	{
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("base");
			modelAndView.addObject("statuses", ProjectStatus.values());
			modelAndView.addObject("id", id);
			modelAndView.addObject("view", "project/project_edit");
			modelAndView.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
			return modelAndView;
		}

		Project currentProject = projectRepository.findOne(id);
		currentProject.setName(project.getName());
		currentProject.setDescription(project.getDescription());
		currentProject.setStatus(project.getStatus());
		projectRepository.flush();

		redir.addFlashAttribute("statement", "Success update Project");

		return new ModelAndView("redirect:/project/");
	}

	@GetMapping("/{project_id}/project_manager")
	public ModelAndView index(@PathVariable("project_id") int projectId)
	{
		ModelAndView modelAndView = new ModelAndView();

		Project project = projectRepository.findOne(projectId);

		modelAndView.addObject("project", project);
		modelAndView.addObject("id", projectId);
		modelAndView.addObject("AllUsers", userRepository.findAll());

		UsersParticipants usersParticipants = new UsersParticipants();
		usersParticipants.setUsers(project.getUsers());
		modelAndView.addObject("usersParticipants", usersParticipants);

		modelAndView.addObject("view", "project/index");
		modelAndView.setViewName("base");

		return modelAndView;
	}

	@PostMapping("/{project_id}/project-participant/add")
	public ModelAndView addParticipant(@PathVariable("project_id") int projectId,
							   @Valid @ModelAttribute UsersParticipants users,
							   BindingResult bindingResult,
							   RedirectAttributes redir)
	{
		Project project = projectRepository.findOne(projectId);

		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("base");
			modelAndView.addObject("id", projectId);
			modelAndView.addObject("view", "project/index");
			modelAndView.addObject("project", project);
			modelAndView.addObject("AllUsers", userRepository.findAll());
			modelAndView.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);

			return modelAndView;
		}

		project.setUsers(users.getUsers());
		projectRepository.flush();
		redir.addFlashAttribute("statement", "Success update Project participants");

		return new ModelAndView("redirect:/project/" + projectId + "/project_manager");
	}
}
