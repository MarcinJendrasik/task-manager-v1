package pl.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.TaskRepository;

import java.security.Principal;

@RestController
@RequestMapping("/backlog")
@PreAuthorize("hasAnyRole('ADMIN','DEVELOPER')")
public class BacklogController {

	private ProjectRepository projectRepository;
	private SprintRepository sprintRepository;
	private TaskRepository taskRepository;

	@Autowired
	BacklogController(ProjectRepository projectRepository,
					  SprintRepository sprintRepository,
					  TaskRepository taskRepository)
	{
		this.projectRepository = projectRepository;
		this.sprintRepository = sprintRepository;
		this.taskRepository = taskRepository;
	}
	
	@GetMapping("/project/{project_id}")
	public ModelAndView get(@PathVariable("project_id") int projectId)
	{
		ModelAndView modelAndView = new ModelAndView();
		
		Project project = projectRepository.findOne(projectId);

		modelAndView.addObject("projectId", projectId);
		modelAndView.addObject("sprint", new Sprint());
		modelAndView.addObject("task", new Task());

		String projectPath = "/project/" + projectId;

		modelAndView.addObject("projectPath", projectPath);
		modelAndView.addObject("urlSaveSprint",projectPath + "/sprint/add");
		modelAndView.addObject("urlUpdateSprint",projectPath + "/sprint/update/");
		modelAndView.addObject("urlDeleteSprint", projectPath + "/sprint/delete/");

		modelAndView.addObject("urlSaveTask", projectPath + "/task/add");
		modelAndView.addObject("urlUpdateTask", projectPath + "/task/update/");
		modelAndView.addObject("urlDeleteTask",  projectPath + "/task/delete/");

		modelAndView.addObject("sprintStatuses", SprintStatus.values());
		modelAndView.addObject("taskStatuses", TaskStatus.values());

		modelAndView.addObject("users", project.getUsers());
		modelAndView.addObject("taskPriority", TaskPriority.values());
		modelAndView.addObject("tasks", taskRepository.findByProjectId(projectId));
		modelAndView.addObject("sprints", sprintRepository.findByProjectId(projectId));

		modelAndView.addObject("view", "backlog/index");
		modelAndView.setViewName("base");

		return modelAndView;
	}
}
