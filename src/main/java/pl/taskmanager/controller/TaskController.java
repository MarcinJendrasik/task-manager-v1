package pl.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.TaskRepository;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.exception.InvalidRequestException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;

@RestController
@RequestMapping("/project/{project_id}/task")
@PreAuthorize("hasAnyRole('ADMIN','DEVELOPER')")
public class TaskController {

    private UserRepository userRepository;
    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;
    private SprintRepository sprintRepository;

    @Autowired
    public TaskController(UserRepository userRepository,
                   ProjectRepository projectRepository,
                   TaskRepository taskRepository,
                   SprintRepository sprintRepository)
    {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.sprintRepository = sprintRepository;
    }

    @GetMapping("/{task_id}")
    public ModelAndView get(@PathVariable("project_id") int projectId,
                            @PathVariable("task_id") int taskId)
    {
        ModelAndView modelAndView = new ModelAndView();

        Task task = taskRepository.findOne(taskId);

        modelAndView.addObject("taskComment", new TaskComment());

        modelAndView.addObject("projectId" ,projectId);

        modelAndView.addObject("sprints", sprintRepository.findByProjectId(projectId));
        modelAndView.addObject("taskPriority", TaskPriority.values());
        modelAndView.addObject("taskStatuses", TaskStatus.values());
        modelAndView.addObject("task", task);

        modelAndView.addObject("users", projectRepository.findOne(projectId).getUsers());
        modelAndView.addObject("taskComments", taskRepository.findOne(taskId).getTaskComments());

        modelAndView.addObject("view", "task/edit");
        modelAndView.setViewName("base");

        return modelAndView;
    }

    @GetMapping(value = "/findOne/{task_id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Task findOne(@PathVariable("task_id") int taskId)
    {
        return taskRepository.findOne(taskId);
    }

    @PostMapping(path = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@PathVariable("project_id") int projectId,
                    Principal principal,
                    @Valid @ModelAttribute Task task,
                    BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid sprint", bindingResult);
        }

        User user = userRepository.findOneByUsername(principal.getName());

        Project project = projectRepository.findOne(projectId);

        task.setProject(project);
        task.setCreatedBy(user);
        task.setCreatedAt(new Date());
        taskRepository.save(task);
    }

    @PutMapping("/update/{task_id}")
    public void update(@PathVariable("task_id") int taskId,
                       @Valid @ModelAttribute Task task,
                       BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid sprint", bindingResult);
        }

        Task currentTask = taskRepository.findById(taskId);
        currentTask.setName(task.getName());
        currentTask.setDescription(task.getDescription());
        currentTask.setAssignee(task.getAssignee());
        currentTask.setEstimate(task.getEstimate());
        currentTask.setPriority(task.getPriority());
        currentTask.setStatus(task.getStatus());
        currentTask.setSprint(task.getSprint());

        taskRepository.flush();
    }

    @PutMapping("/update/{task_id}/status")
    public void updateStatus(@PathVariable("task_id") int taskId,
                             @RequestParam("status") TaskStatus status)
    {
        Task currentTask = taskRepository.findOne(taskId);
        currentTask.setStatus(status);
        taskRepository.flush();
    }

    @DeleteMapping("/delete/{task_id}")
    public void delete(@PathVariable("task_id") int taskId)
    {
        Task task = taskRepository.findById(taskId);
        taskRepository.delete(task);
        taskRepository.flush();
    }
}
