package pl.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.*;
import pl.taskmanager.exception.InvalidRequestException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@RestController
@RequestMapping("/project/{project_id}/sprint")
@PreAuthorize("hasAnyRole('ADMIN','DEVELOPER')")
public class SprintController {

    private UserRepository userRepository;
    private ProjectRepository projectRepository;
    private SprintRepository sprintRepository;

    @Autowired
    SprintController(UserRepository userRepository,
                    ProjectRepository projectRepository,
                    SprintRepository sprintRepository)
    {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.sprintRepository = sprintRepository;
    }

    @GetMapping("/{sprint_id}")
    public ModelAndView get(@PathVariable("sprint_id") int sprintId,
                            @PathVariable("project_id") int projectId)
    {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("task", new Task());
        modelAndView.addObject("taskComment", new TaskComment());

        modelAndView.addObject("sprintId", sprintId);

        modelAndView.addObject("projectId", projectId);
        modelAndView.addObject("urlUpdateTaskStatus","/project/" + projectId + "/task/update/");
        modelAndView.addObject("urlSaveTask","/project/" + projectId + "/task/add");
        modelAndView.addObject("urlUpdateTask","/project/" + projectId + "/task/update/");
        modelAndView.addObject("urlDeleteTask", "/project/" + projectId + "/task/delete/");

        Sprint sprint = sprintRepository.findOne(sprintId);

        modelAndView.addObject("sprint", sprint);
        modelAndView.addObject("sprints", sprintRepository.findByProjectId(projectId));
        modelAndView.addObject("tasks", sprint.getTasks());
        modelAndView.addObject("taskPriority", TaskPriority.values());
        modelAndView.addObject("taskStatuses", TaskStatus.values());
        modelAndView.addObject("users", userRepository.findAll());

        modelAndView.addObject("view", "sprint/index");
        modelAndView.setViewName("base");

        return modelAndView;
    }

    @GetMapping(value = "/findOne/{sprint_id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Sprint findOne(@PathVariable("sprint_id") int sprintId)
    {
        return sprintRepository.findOne(sprintId);
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@PathVariable("project_id") int projectId,
                    @Valid @ModelAttribute Sprint sprint,
                    BindingResult bindingResult,
                    Principal principal)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid sprint", bindingResult);
        }

        String name = principal.getName();
        User user = userRepository.findOneByUsername(name);

        Project project = projectRepository.findOne(projectId);

        sprint.setProject(project);
        sprint.setUser(user);
        sprint.setCreatedAt(new Date());
        sprintRepository.save(sprint);

        Set<Sprint> sprintList = new HashSet<Sprint>();
        sprintList.add(sprint);
        project.setSprints(sprintList);
        projectRepository.flush();
    }

    @PutMapping("/update/{sprint_id}")
    public void update(@PathVariable("sprint_id") int id,
                       @Valid @ModelAttribute Sprint sprint,
                       BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid sprint", bindingResult);
        }

        Sprint currentSprint = sprintRepository.findOne(id);
        currentSprint.setName(sprint.getName());
        currentSprint.setGoal(sprint.getGoal());
        currentSprint.setStatus(sprint.getStatus());
        sprintRepository.flush();
    }

    @PutMapping("/update/dateStop/{sprint_id}")
    public void updateDateStop(@PathVariable("sprint_id") int id)
    {
        Sprint currentSprint = sprintRepository.findOne(id);
        currentSprint.setDateStop(new Date());
        sprintRepository.flush();
    }

    @DeleteMapping("/delete/{sprint_id}")
    public void delete(@PathVariable("sprint_id") int sprintId)
    {
        sprintRepository.delete(sprintId);
        sprintRepository.flush();
    }
}
