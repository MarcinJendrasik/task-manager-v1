package pl.taskmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.taskmanager.entity.Task;
import pl.taskmanager.entity.TaskComment;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.TaskCommentRepository;
import pl.taskmanager.repository.TaskRepository;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.exception.InvalidRequestException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/task/{task_id}/comment")
@PreAuthorize("hasAnyRole('ADMIN','DEVELOPER')")
public class TaskCommentController {

    private UserRepository userRepository;
    private TaskCommentRepository taskCommentRepository;
    private TaskRepository taskRepository;

    TaskCommentController(UserRepository userRepository,
                          TaskCommentRepository taskCommentRepository,
                          TaskRepository taskRepository)
    {
        this.userRepository = userRepository;
        this.taskCommentRepository = taskCommentRepository;
        this.taskRepository = taskRepository;
    }

    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<TaskComment> findAll(@PathVariable("task_id") int taskId)
    {
        return taskCommentRepository.findAllByTaskId(taskId);
    }

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@PathVariable("task_id") int taskId,
                            Principal principal,
                            @Valid @ModelAttribute TaskComment taskComment,
                            BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid task comment", bindingResult);
        }

        User user = userRepository.findOneByUsername(principal.getName());

        Task task = taskRepository.findOne(taskId);
        taskComment.setUser(user);
        taskComment.setTask(task);
        taskComment.setCreatedAt(new Date());

        taskCommentRepository.save(taskComment);

        Set<TaskComment> taskCommentList = new HashSet<>();
        taskCommentList.add(taskComment);

        task.setTaskComments(taskCommentList);
        task.setCreatedAt(new Date());

        taskRepository.flush();
    }

    @PutMapping(value = "/update/{task_comment_id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public void update(@PathVariable("task_comment_id") int taskCommentId,
                       @Valid @ModelAttribute TaskComment taskComment,
                       BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Invalid task comment", bindingResult);
        }

        TaskComment currentTaskComment = taskCommentRepository.findOne(taskCommentId);
        currentTaskComment.setComment(taskComment.getComment());
        taskCommentRepository.flush();
    }

    @DeleteMapping("/delete/{task_comment_id}")
    public void delete(@PathVariable("task_comment_id") int taskCommentId)
    {
        taskCommentRepository.delete(taskCommentId);
        taskCommentRepository.flush();
    }
}
