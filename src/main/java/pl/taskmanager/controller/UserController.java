package pl.taskmanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.taskmanager.entity.User;
import pl.taskmanager.repository.UserRepository;
import pl.taskmanager.repository.UserRoleRepository;

import javax.validation.Valid;

@RestController
@RequestMapping("/secured/user")
@PreAuthorize("hasAnyRole('ADMIN')")
public class UserController {

	private UserRepository userRepository;
	private UserRoleRepository userRoleRepository;
	private PasswordEncoder passwordEncoder;

	@Autowired
	public UserController(UserRepository userRepository, UserRoleRepository userRoleRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.userRoleRepository = userRoleRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@GetMapping
	@RequestMapping(value = "/")
	public ModelAndView findAll(@RequestParam(defaultValue = "1") int page)
	{
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("users", userRepository.findAll(new PageRequest(page-1, 10)));
		modelAndView.addObject("currentPage", page);
		modelAndView.addObject("view", "user/user_list");

		modelAndView.setViewName("base");
		return modelAndView;
	}

	@GetMapping(value = "/add")
	public ModelAndView add()
	{
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.addObject("user", new User());
		modelAndView.addObject("allUserRoles",userRoleRepository.findAll());

		modelAndView.addObject("view","user/user_add");
		modelAndView.setViewName("base");

		return modelAndView;
	}

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView add(@Valid @ModelAttribute User user,
					  		BindingResult bindingResult,
							RedirectAttributes redir)
	{
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("base");
			modelAndView.addObject("user", user);
			modelAndView.addObject("allUserRoles", userRoleRepository.findAll());
			modelAndView.addObject("view", "user/user_add");
			modelAndView.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
			return modelAndView;
		}

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		userRepository.save(user);
		redir.addFlashAttribute("statement", "Success create User");

		return new ModelAndView("redirect:/secured/user/");
	}

	@GetMapping(value = "/update/{id}")
	public ModelAndView edit(@PathVariable("id") int id)
	{
		ModelAndView modelAndView = new ModelAndView();

		User user = userRepository.findOne(id);
		modelAndView.addObject("user", user);
		modelAndView.addObject("allUserRoles", userRoleRepository.findAll());

		modelAndView.addObject("view","user/user_edit");
		modelAndView.setViewName("base");

		return modelAndView;
	}
	
	@PutMapping(value = "/update/{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView update(@PathVariable("id") int id,
						 @Valid @ModelAttribute User user,
						 BindingResult bindingResult,
				   		 RedirectAttributes redir)
	{
		if (bindingResult.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView("base");
			modelAndView.addObject("view", "user/user_edit");
			modelAndView.addObject("id", id);
			modelAndView.addObject("user", user);
			modelAndView.addObject("allUserRoles", userRoleRepository.findAll());
			modelAndView.setStatus(HttpStatus.UNPROCESSABLE_ENTITY);
			return modelAndView;
		}

		User currentUser = userRepository.findOne(id);
		currentUser.setUsername(user.getUsername());
		currentUser.setEmail(user.getEmail());

		if (!currentUser.getPassword().equals(user.getPassword())) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}

        currentUser.setPassword(user.getPassword());
		currentUser.setUserRoles(user.getUserRoles());
		userRepository.flush();

		redir.addFlashAttribute("statement", "Success update User");

		return new ModelAndView("redirect:/secured/user/");
	}

	@GetMapping("/update/status/{user_id}")
	public ModelAndView updateStatus(@PathVariable("user_id") int id,
									 RedirectAttributes redir)
	{
		User user = userRepository.findOne(id);

		if (user.isEnable())
			user.setEnable(false);
		else
			user.setEnable(true);

		userRepository.flush();

		redir.addFlashAttribute("statement", "Success update User status");

		return new ModelAndView("redirect:/secured/user/");
	}
}
