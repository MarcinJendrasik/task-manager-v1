package pl.taskmanager.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import pl.taskmanager.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findOneByUsername(String username);
}
