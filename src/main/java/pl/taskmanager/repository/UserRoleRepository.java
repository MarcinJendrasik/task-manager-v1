package pl.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.taskmanager.entity.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Integer>{

}
