package pl.taskmanager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.taskmanager.entity.Sprint;

public interface SprintRepository extends JpaRepository<Sprint, Integer> {

	List<Sprint> findByProjectId(int project_id);
}
