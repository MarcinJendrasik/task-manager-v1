package pl.taskmanager.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.taskmanager.entity.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {

	List<Task> findByProjectId(int projectId);
	Task findById(int taskId);
}
