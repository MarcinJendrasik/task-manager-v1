package pl.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.taskmanager.entity.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{

}
