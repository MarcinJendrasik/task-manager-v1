package pl.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.taskmanager.entity.TaskComment;

import java.util.Set;

public interface TaskCommentRepository extends JpaRepository<TaskComment, Integer> {

    Set<TaskComment> findAllByTaskId(int taskId);
}
