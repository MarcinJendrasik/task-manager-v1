package pl.taskmanager.binding;

import java.util.List;

public class ErrorResource {

    private String code;
    private String message;
    private List<FieldErrorResource> fieldErrorResources;

    public ErrorResource()
    {

    }

    public ErrorResource(String code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FieldErrorResource> getFieldErrorResources() {
        return fieldErrorResources;
    }

    public void setFieldErrorResources(List<FieldErrorResource> fieldErrorResources) {
        this.fieldErrorResources = fieldErrorResources;
    }
}
