$(function() {

    $('form#task-form').submit(function (event) {
        event.preventDefault();

        var form = $('form#task-form');

        $.ajax({
            method: "PUT",
            data: $(this).serialize(),
            url: $(this).attr('action'),
            success: function () {
                toastr.success("Success update Task", "Update Task");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(form, message);
            }
        });
    });

    $('#comment-form').submit(function (event) {
        event.preventDefault();

        var form = $('form#comment-form');

        $.post({
            data: $(this).serialize(),
            url: $(this).attr('action'),
            success: function () {
                toastr.success("Success comment Task", "Comment Task");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(form, message);
            }
        });
    });

    $('.save-comment').click(function (event) {
        event.preventDefault();

        var TaskCommentId = $(this).data('task-comment-id');
        var form = $(this).closest('form#comment-form-' + TaskCommentId);

        $.ajax({
            method: "PUT",
            data: form.serialize(),
            url: form.attr('action'),
            success: function () {
                toastr.success("Success update Task", "Update Task");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(form, message);
            }
        });

    });

    $('.edit-task-comment').click(function (event) {

        event.preventDefault();
        $(this).addClass('hidden');
        var formGroup = $(this).closest('.form-group');
        formGroup.find('textarea').removeAttr('disabled');
        formGroup.find('.save-comment').removeClass('hidden');
        formGroup.find('.cancel-edit-comment').removeClass('hidden');
    });

    $('.cancel-edit-comment').click(function (event) {

        event.preventDefault();
        location.reload();
    });

    $('.delete-comment').click(function (event) {
        event.preventDefault();

        var that = $(this);

        toastr.warning("Are you sure you want delete Task comment?" +
            "<button id='confirm-yes' class='btn btn-default'>Yes</button>" +
            "<button id='confirm-no' class='btn btn-default'>No</button><br>",
            "Delete Task comment?",
            {
                closeButton: false,
                allowHtml: true,
                onShown: function (toast) {

                    $("#confirm-yes").click(function () {

                        $.ajax({
                            method: "DELETE",
                            url: that.attr('href'),
                            success: function () {
                                toastr.success("Success delete Task comment", "Delete Task comment");

                                setTimeout(function(){
                                    location.reload()
                                }, timeout);
                            },
                            error: function () {
                                toastr.error("Occurred error", "Error");
                            }
                        });

                    })
                }
            }
        )
    });
});