$(function() {

    $(".TO_DO, .IN_PROGRESS, .DONE").click(function () {

        var that = $(this);
        var status = that.attr('class');

        $.ajax({
            method: "PUT",
            url: urlUpdateTaskStatus + that.data("id") + "/status/",
            data: {
                status: status
            },
            success: function () {
                location.reload();
            }
        });
    });
});