$(function () {

    var itemToUpdate;

    var modalAddTask = $("#modal-add-task");
    var modalAddTaskViewComment = modalAddTask.find("a.view-comment");
    var modalAddTaskTitle = modalAddTask.find(".modal-title");
    var modalAddTaskInputName = modalAddTask.find("input#name");
    var modalAddTaskTextareaDescription = modalAddTask.find("textarea#description");
    var modalAddTaskInputAssignee = modalAddTask.find("select#assignee");
    var modalAddTaskInputEstimate = modalAddTask.find("input#estimate");
    var modalAddTaskSelectPriority = modalAddTask.find("select#priority");
    var modalAddTaskInputStatus = modalAddTask.find("select#status");
    var modalAddTaskSelectSprint = modalAddTask.find("select#sprint");

    $("button.task-delete").click(function () {

        var that = $(this);

        toastr.warning("Are you sure you want delete Task?" +
            "<button id='confirm-yes' class='btn btn-default'>Yes</button>" +
            "<button id='confirm-no' class='btn btn-default'>No</button>",
            "Delete Task?",
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {

                $("#confirm-yes").click(function() {

                    $.ajax({
                        method: "DELETE",
                        url: urlDeleteTask + that.data('task-id'),
                        success: function () {
                            toastr.success("Success remove Task", "Remove Task");

                            setTimeout(function(){
                                location.reload()
                            }, timeout);
                        },
                        error: function () {
                            toastr.error("Occurred error", "Error");
                        }
                    });
                })
            }
        })

    });

    $("button#add-task").click(function () {

        modalAddTaskViewComment.addClass("hidden");
        modalAddTaskTitle.text("Add task");
        modalAddTask.find("button#save-task").removeClass('hidden');
        modalAddTask.find("button#update-task").addClass('hidden');

        modalAddTask.modal('show');
        modalAddTask.find('.error').remove();

        modalAddTaskInputName.val('');
        modalAddTaskTextareaDescription.val('');
        modalAddTaskInputAssignee.val('');
        modalAddTaskInputEstimate.val('');
        modalAddTaskSelectPriority.val('');
        modalAddTaskInputStatus.val('');
        modalAddTaskSelectSprint.val('');
    });

    $("button.edit-task").click(function () {

        var that = $(this);

        modalAddTaskViewComment.removeClass("hidden");
        modalAddTaskTitle.text("Edit task");
        modalAddTask.find("button#save-task").addClass('hidden');
        modalAddTask.find("button#update-task").removeClass('hidden');

        modalAddTask.modal('show');
        modalAddTask.find('.error').remove();

        $.get({
            url: $(this).attr('href'),
            success: function (task) {

                modalAddTaskInputName.val(task.name);
                modalAddTaskTextareaDescription.val(task.description);
                modalAddTaskInputAssignee.val(that.data('assignee-id'));
                modalAddTaskInputEstimate.val(task.estimate);
                modalAddTaskSelectPriority.val(task.priority);
                modalAddTaskInputStatus.val(task.status);
                modalAddTaskSelectSprint.val(that.data('sprint-id'));

                itemToUpdate = task.id;
            }
        });
    });

    $("button#save-task").click(function () {

        $.post({
            url: urlSaveTask,
            data: $("form#task-form").serialize(),
            success: function () {
                toastr.success("Success save Task", "Save Task");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(modalAddTask, message);
            }
        });
    });

    $("button#update-task").click(function () {

        $.ajax({
            method: "PUT",
            url: urlUpdateTask + itemToUpdate,
            data: $("form#task-form").serialize(),
            success: function () {
                toastr.success("Success update Task", "Update Task");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(modalAddTask, message)
            }
        });
    });

    modalAddTaskViewComment.click(function (event) {
        event.preventDefault();

        window.location = $(this).attr("href") + itemToUpdate;
    });

});