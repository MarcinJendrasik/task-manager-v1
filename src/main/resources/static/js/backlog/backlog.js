$(function() {

    var modalAddSprint = $("#modal-add-sprint");
    var modalAddSprintTitle = modalAddSprint.find(".modal-title");
    var modalAddSprintInputName = modalAddSprint.find("input#name");
    var modalAddSprintTextareaGoal = modalAddSprint.find("textarea#goal");
    var modalAddSprintInputStatus = modalAddSprint.find("select#status");

    var itemToUpdate;

    $("button.sprint-delete").click(function () {

        var that = $(this);

        toastr.warning("Are you sure you want delete Sprint?" +
            "<button id='confirm-yes' class='btn btn-default'>Yes</button>" +
            "<button id='confirm-no' class='btn btn-default'>No</button>",
            "Delete Sprint?",
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {

                $("#confirm-yes").click(function(){

                    $.ajax({
                        method: "DELETE",
                        url : urlDeleteSprint + that.data('sprint-id'),
                        success: function () {
                            toastr.success("Success remove Sprint", "Remove Sprint");

                            setTimeout(function(){
                                location.reload()
                            }, timeout);
                        },
                        error: function () {
                            toastr.error("Occurred error","Error");
                        }
                    });
                });
            }
        });

    });

    $("button#add-sprint").click(function () {

        modalAddSprint.find('.error').remove();
        modalAddSprintTitle.text("Add Sprint");
        modalAddSprint.find("button#save-sprint").removeClass('hidden');

        modalAddSprint.find("button#update-sprint").addClass('hidden');
        modalAddSprint.modal('show');

        modalAddSprintInputName.val('');
        modalAddSprintTextareaGoal.val('');
        modalAddSprintInputStatus.val('');
    });

    $("button#edit-sprint").click(function () {

        modalAddSprint.find('.error').remove();
        modalAddSprintTitle.text("Edit Sprint");
        modalAddSprint.find("button#save-sprint").addClass('hidden');
        modalAddSprint.find("button#update-sprint").removeClass('hidden');

        modalAddSprint.modal('show');

        $.get({
            url: $(this).attr('href'),
            success: function (sprint) {
                modalAddSprintInputName.val(sprint.name);
                modalAddSprintTextareaGoal.val(sprint.goal);
                modalAddSprintInputStatus.val(sprint.status);
                itemToUpdate = sprint.id;
            }
        });
    });

    $("button#save-sprint").click(function () {

        $.post({
            url: urlSaveSprint,
            data: $("form#sprint-form").serialize(),
            success: function () {
                toastr.success("Success save Sprint", "Save Sprint");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(modalAddSprint, message);
            }
        });
    });

    $("button#update-sprint").click(function () {

        $.ajax({
            method: "PUT",
            url: urlUpdateSprint + itemToUpdate,
            data: $("form#sprint-form").serialize(),
            success: function () {
                toastr.success("Success update Sprint", "Update Sprint");

                setTimeout(function(){
                    location.reload()
                }, timeout);
            },
            error: function (message) {
                handleError(modalAddSprint, message);
            }
        });
    });

});