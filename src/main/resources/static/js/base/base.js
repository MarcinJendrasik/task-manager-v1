var timeout = 3000;

function handleError(container, message) {

    container.find('.error').remove();
    $(message.responseJSON.fieldErrorResources).each(function (ind, el) {

        container
            .find("input[name="+`${el.field}`+"] ," +
                " textarea[name="+`${el.field}`+"] ," +
                " select[name="+`${el.field}`+"]")
            .after('<p class="error">' + el.message + '</p>');

    });
}