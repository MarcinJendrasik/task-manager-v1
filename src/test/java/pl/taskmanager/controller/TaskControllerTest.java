package pl.taskmanager.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.taskmanager.controller.TaskController;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.TaskRepository;
import pl.taskmanager.repository.UserRepository;

import java.security.Principal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TaskController.class)
@WithMockUser(username = "test")
public class TaskControllerTest {

    @MockBean
    private TaskRepository taskRepository;

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private SprintRepository sprintRepository;

    @MockBean
    private Principal principal;

    @MockBean
    private BindingResult bindingResult;

    @Autowired
    private MockMvc mvc;

    private static final int projectId = 1;
    private static final int taskId = 2;
    private static final int sprintId = 3;
    private static final int userId = 4;

    @Before
    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldPrepareDataToViewEditTaskAndComment() throws Exception
    {
        Task task = new Task();
        task.setSprint(new Sprint());
        Project project = new Project();

        when(projectRepository.findOne(projectId)).thenReturn(project);
        when(taskRepository.findOne(taskId)).thenReturn(task);

        Set<TaskComment> taskComments = new HashSet<TaskComment>();
        TaskComment taskComment = new TaskComment();
        taskComment.setUser(new User());
        taskComments.add(taskComment);

        task.setTaskComments(taskComments);

        assertEquals(task.getTaskComments(), taskComments);

        mvc.perform(get("/project/{project_id}/task/{task_id}", projectId, taskId))
                .andExpect(status().isOk())
                .andExpect(view().name("base"))
                .andExpect(model().attribute("taskComment", hasProperty("id", is(0))))
                .andExpect(model().attribute("taskComment", hasProperty("comment", isEmptyOrNullString())))
                .andExpect(model().attribute("projectId", projectId))
                .andExpect(model().attribute("sprints", sprintRepository.findByProjectId(projectId)))
                .andExpect(model().attribute("taskStatuses", TaskStatus.values()))
                .andExpect(model().attribute("task", task))
                .andExpect(model().attribute("taskComments", taskRepository.findOne(taskId).getTaskComments()))
                .andExpect(model().attribute("view", "task/edit"));
    }

    @Test
    public void shouldReturnTaskById() throws Exception
    {
        Task task = new Task();
        task.setName("task test");
        task.setDescription("some description");
        task.setStatus(TaskStatus.TO_DO);
        task.setPriority(TaskPriority.MEDIUM);
        task.setEstimate(2);
        task.setParentId(0);

        when(taskRepository.findOne(taskId)).thenReturn(task);

        mvc.perform(get("/project/{project_id}/task/findOne/{task_id}",projectId, taskId))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.name", is("task test")))
                .andExpect(jsonPath("$.description", is("some description")))
                .andExpect(jsonPath("$.status", is(TaskStatus.TO_DO.name())))
                .andExpect(jsonPath("$.priority", is(TaskPriority.MEDIUM.name())))
                .andExpect(jsonPath("$.estimate", is(2)));

        verify(taskRepository, times(1)).findOne(taskId);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    public void shouldCreateTask() throws Exception
    {
        Sprint sprint = new Sprint(sprintId);
        Project project = new Project();

        when(projectRepository.findOne(projectId)).thenReturn(project);

        User user = new User(userId);

        when(userRepository.findOneByUsername(any())).thenReturn(user);

        Task task = new Task(taskId);
        task.setName("task test");
        task.setDescription("some description");
        task.setStatus(TaskStatus.TO_DO);
        task.setPriority(TaskPriority.MEDIUM);
        task.setEstimate(2);
        task.setParentId(0);
        task.setSprint(sprint);
        task.setAssignee(user);
        task.setCreatedAt(new Date());

        mvc.perform(
                post("/project/{project_id}/task/add", projectId)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .flashAttr("task", task)
                )
                .andExpect(status().isCreated())
                .andReturn();

        verify(taskRepository, times(1)).save(task);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    public void shouldUpdateTask() throws Exception
    {
        Sprint sprint = new Sprint(sprintId);
        User user = new User(userId);

        Task task = new Task(taskId);
        task.setName("task test");
        task.setDescription("some description");
        task.setStatus(TaskStatus.TO_DO);
        task.setPriority(TaskPriority.MEDIUM);
        task.setEstimate(2);
        task.setParentId(0);
        task.setSprint(sprint);
        task.setAssignee(user);

        when(taskRepository.findById(taskId)).thenReturn(task);

        mvc.perform(
                put("/project/{project_id}/task/update/{task_id}", projectId, taskId)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .flashAttr("task", task)
                )
                .andExpect(status().isOk());

        verify(taskRepository, times(1)).findById(task.getId());
        verify(taskRepository, times(1)).flush();
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    public void shouldThrowExceptionIfFieldIsEmpty() throws Exception
    {
        Sprint sprint = new Sprint(sprintId);
        User user = new User(userId);

        Task task = new Task(taskId);
        task.setName("task test");
        task.setDescription("some description");
        task.setStatus(TaskStatus.TO_DO);
        task.setPriority(TaskPriority.MEDIUM);
        task.setEstimate(2);
        task.setParentId(0);
        task.setSprint(sprint);
        task.setAssignee(user);

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                post("/project/{project_id}/task/add", projectId)
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                        .param("name", task.getName())
                        .param("description",  task.getDescription())
                        .param("status", task.getStatus().name())
                        .param("priority", task.getPriority().name())
                        .param("estimate", ""+task.getEstimate())
                        .param("parentId", ""+task.getParentId())
                        .param("sprint.id", ""+task.getSprint().getId())
                )
                .andExpect(status().is(422))
                .andExpect(jsonPath("$.code", is("InvalidRequest")))
                .andExpect(jsonPath("$.message", is("Invalid sprint")))
                .andExpect(jsonPath("$.fieldErrorResources[0].resource", is("task")))
                .andExpect(jsonPath("$.fieldErrorResources[0].field", is("assignee")))
                .andExpect(jsonPath("$.fieldErrorResources[0].message", is("may not be empty")));
    }

    @Test
    public void shouldDeleteTask() throws Exception
    {
        Task task = new Task(taskId);

        when(taskRepository.findById(taskId)).thenReturn(task);
        doNothing().when(taskRepository).delete(task);

        mvc.perform(
                delete("/project/{project_id}/task/delete/{task_id}", projectId, taskId)
                .param("task_id", ""+taskId)
            )
            .andExpect(status().isOk());

        verify(taskRepository, times(1)).findById(taskId);
        verify(taskRepository, times(1)).delete(task);
        verify(taskRepository, times(1)).flush();
        verifyNoMoreInteractions(taskRepository);
    }

}
