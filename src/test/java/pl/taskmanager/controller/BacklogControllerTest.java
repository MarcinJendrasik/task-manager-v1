package pl.taskmanager.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.SprintRepository;
import pl.taskmanager.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = BacklogController.class)
@WithMockUser(username = "test")
public class BacklogControllerTest {

    @MockBean
    ProjectRepository projectRepository;

    @MockBean
    SprintRepository sprintRepository;

    @MockBean
    TaskRepository taskRepository;

    @Autowired
    private MockMvc mvc;

    private static final int projectId = 1;

    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldPrepareDataToViewBacklog() throws Exception
    {
        Project project = new Project();

        User user = new User();

        Sprint sprint = new Sprint();
        sprint.setUser(user);

        when(projectRepository.findOne(projectId)).thenReturn(project);

        List<Task> tasks = new ArrayList<Task>(Arrays.asList(new Task()));
        List<Sprint> sprints = new ArrayList<Sprint>(Arrays.asList(sprint));

        when(taskRepository.findByProjectId(projectId)).thenReturn(tasks);
        when(sprintRepository.findByProjectId(projectId)).thenReturn(sprints);

        String projectPath = "/project/" + projectId;

        mvc.perform(
                get("/backlog/project/{project_id}", projectId)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "backlog/index"))
            .andExpect(model().attribute("projectId", projectId))

            .andExpect(model().attribute("sprint", hasProperty("id", is(0))))
            .andExpect(model().attribute("sprint", hasProperty("name", isEmptyOrNullString())))
            .andExpect(model().attribute("sprint", hasProperty("goal", isEmptyOrNullString())))
            .andExpect(model().attribute("sprint", hasProperty("status", isEmptyOrNullString())))

            .andExpect(model().attribute("task", hasProperty("id", is(0))))
            .andExpect(model().attribute("task", hasProperty("name", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("description", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("assignee", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("estimate", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("priority", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("sprint", isEmptyOrNullString())))
            .andExpect(model().attribute("task", hasProperty("status", isEmptyOrNullString())))

            .andExpect(model().attribute("projectPath", projectPath))
            .andExpect(model().attribute("urlSaveSprint",projectPath + "/sprint/add"))
            .andExpect(model().attribute("urlUpdateSprint",projectPath + "/sprint/update/"))
            .andExpect(model().attribute("urlDeleteSprint", projectPath + "/sprint/delete/"))
            .andExpect(model().attribute("urlSaveTask", projectPath + "/task/add"))
            .andExpect(model().attribute("urlUpdateTask", projectPath + "/task/update/"))
            .andExpect(model().attribute("urlDeleteTask",  projectPath + "/task/delete/"))
            .andExpect(model().attribute("sprintStatuses", SprintStatus.values()))
            .andExpect(model().attribute("taskStatuses", TaskStatus.values()))
            .andExpect(model().attribute("users", project.getUsers()))
            .andExpect(model().attribute("taskPriority", TaskPriority.values()))
            .andExpect(model().attribute("tasks", tasks))
            .andExpect(model().attribute("sprints", sprints));

        verify(projectRepository, times(1)).findOne(projectId);
        verify(taskRepository, times(1)).findByProjectId(projectId);
        verify(sprintRepository, times(1)).findByProjectId(projectId);
        verifyNoMoreInteractions(projectRepository);
    }
}
