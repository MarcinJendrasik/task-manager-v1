package pl.taskmanager.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.taskmanager.entity.*;
import pl.taskmanager.repository.ProjectRepository;
import pl.taskmanager.repository.UserRepository;

import java.util.*;

import static org.mockito.MockitoAnnotations.initMocks;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProjectController.class)
@WithMockUser(username = "test")
public class ProjectControllerTest {

    @MockBean
    private ProjectRepository projectRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BindingResult bindingResult;

    @Autowired
    private MockMvc mvc;

    private final static int projectId = 1;
    private final static int userId = 1;

    @Before
    public void setUp()
    {
        initMocks(this);
    }

    @Test
    public void shouldPrepareDataToViewProjectListByAdmin() throws Exception
    {
        int page = 1;
        int size = 10;
        int totalElements = 10;

        UserRole userRole = new UserRole();
        userRole.setName(UserRoleType.ROLE_ADMIN);

        Set<UserRole> userRoles = new HashSet<UserRole>(Arrays.asList(userRole));

        User user = new User();
        user.setUserRoles(userRoles);
        user.setUsername("test");

        Project project = new Project();
        project.setCreatedBy(user);

        List<Project> projects = new ArrayList<Project>(Arrays.asList(project));

        when(userRepository.findOneByUsername(user.getUsername())).thenReturn(user);
        when(projectRepository.findAll()).thenReturn(projects);

        mvc.perform(
                get("/project/")
                        .param("page", ""+page)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("base"))
                .andExpect(model().attribute("currentPage", page))
                .andExpect(model().attribute("projects", projects))
                .andExpect(model().attribute("view", "project/project_list"));

        verify(userRepository, times(1)).findOneByUsername(user.getUsername());
        verify(projectRepository, times(1)).findAll();
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldPrepareDataToViewProjectListByDeveloper() throws Exception
    {
        int page = 1;
        int size = 10;
        int totalElements = 10;

        UserRole userRole = new UserRole();
        userRole.setName(UserRoleType.ROLE_DEVELOPER);

        Set<UserRole> userRoles = new HashSet<UserRole>(Arrays.asList(userRole));

        User user = new User();
        user.setUserRoles(userRoles);
        user.setUsername("test");

        when(userRepository.findOneByUsername(user.getUsername())).thenReturn(user);

        mvc.perform(
                get("/project/")
                        .param("page", ""+page)
        )
                .andExpect(status().isOk())
                .andExpect(view().name("base"))
                .andExpect(model().attribute("currentPage", page))
                .andExpect(model().attribute("projects", user.getProjects()))
                .andExpect(model().attribute("view", "project/project_list"));

        verify(userRepository, times(1)).findOneByUsername(user.getUsername());
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void shouldPrepareDataToViewProjectAdd() throws Exception
    {
        mvc.perform(
                get("/project/add")
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "project/project_add"))

            .andExpect(model().attribute("project", hasProperty("id", is(0))))
            .andExpect(model().attribute("project", hasProperty("name", isEmptyOrNullString())))
            .andExpect(model().attribute("project", hasProperty("description", isEmptyOrNullString())))
            .andExpect(model().attribute("project", hasProperty("status", isEmptyOrNullString())))

            .andExpect(model().attribute("statuses", ProjectStatus.values()));
    }

    @Test
    public void shouldCreateProject() throws Exception
    {
        User user = new User(userId);
        user.setUsername("test");

        Project project = new Project();
        project.setName("test");
        project.setDescription("test");
        project.setStatus(ProjectStatus.ACTIVE);
        project.setCreatedBy(user);

        when(userRepository.findOneByUsername(user.getUsername())).thenReturn(user);
        when(projectRepository.save(project)).thenReturn(project);

        mvc.perform(
                post("/project/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("project", project)
            )
            .andExpect(redirectedUrl("/project/"))
            .andExpect(flash().attribute("statement","Success create Project"));

        verify(userRepository, times(1)).findOneByUsername(user.getUsername());
        verify(projectRepository, times(1)).save(project);
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    public void cannotCreateProjectWhenFieldIsEmptyThrowException() throws Exception
    {
        Project project = new Project();
        project.setName("test");
        project.setStatus(ProjectStatus.ACTIVE);

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                post("/project/add")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("project", project)
            )
            .andExpect(status().is(422))
            .andExpect(model().hasErrors())
            .andExpect(view().name("base"))
            .andExpect(model().attributeHasErrors("project"))
            .andExpect(model().attribute("statuses", ProjectStatus.values()))
            .andExpect(model().attribute("view", "project/project_add"));
    }

    @Test
    public void shouldPrepareDataToViewProjectUpdate() throws Exception
    {
        Project project = new Project();
        project.setName("test");
        project.setDescription("test");
        project.setStatus(ProjectStatus.ACTIVE);
        project.setCreatedBy(new User(userId));

        when(projectRepository.findOne(projectId)).thenReturn(project);

        mvc.perform(
                get("/project/update/{id}", projectId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("project", project)
            )
            .andExpect(status().isOk())
            .andExpect(model().attribute("id", projectId))
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "project/project_edit"))
            .andExpect(model().attribute("project", project))
            .andExpect(model().attribute("statuses", ProjectStatus.values()));

        verify(projectRepository, times(1)).findOne(projectId);
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    public void shouldUpdateProject() throws Exception
    {
        Project project = new Project();
        project.setName("test");
        project.setDescription("test");
        project.setStatus(ProjectStatus.ACTIVE);
        project.setCreatedBy(new User((userId)));

        when(projectRepository.findOne(projectId)).thenReturn(project);
        doNothing().when(projectRepository).flush();

        mvc.perform(
                put("/project/update/{id}", projectId)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .flashAttr("project", project)
            )
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/project/"))
            .andExpect(flash().attribute("statement","Success update Project"));

        verify(projectRepository, times(1)).findOne(projectId);
        verify(projectRepository, times(1)).flush();
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    public void cannotUpdateProjectWhenFieldIsEmptyThrowException() throws Exception
    {
        Project project = new Project();
        project.setStatus(ProjectStatus.ACTIVE);

        when(bindingResult.hasErrors()).thenReturn(true);

        mvc.perform(
                put("/project/update/{id}", projectId)
                    .contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                    .flashAttr("project", project)
            )
            .andExpect(status().is(422))
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasErrors("project"))
            .andExpect(view().name("base"))
            .andExpect(model().attribute("view", "project/project_edit"))
            .andExpect(model().attribute("id", projectId))
            .andExpect(model().attribute("statuses", ProjectStatus.values()));
    }

    @Test
    public void shouldPrepareDataToViewProjectManager() throws Exception
    {
        Project project = new Project();
        List<User> list = new ArrayList<>();

        when(projectRepository.findOne(projectId)).thenReturn(project);
        when(userRepository.findAll()).thenReturn(list);

        mvc.perform(
                get("/project/{project_id}/project_manager", projectId)
            )
            .andExpect(status().isOk())
            .andExpect(view().name("base"))
            .andExpect(model().attribute("project", project))
            .andExpect(model().attribute("id", projectId))
            .andExpect(model().attribute("AllUsers", list))

            .andExpect(model().attribute("usersParticipants", hasProperty("users", emptyCollectionOf(User.class))))
            .andExpect(model().attribute("view", "project/index"));

        verify(projectRepository, times(1)).findOne(projectId);
        verify(userRepository, times(1)).findAll();
        verifyNoMoreInteractions(projectRepository, userRepository);
    }

    @Test
    public void shouldAssigneeProjectParticipant() throws Exception
    {
        Project project = new Project();

        Set<User> users = new HashSet<User>();
        users.add(new User());

        UsersParticipants usersParticipants = new UsersParticipants();
        usersParticipants.setUsers(users);

        when(projectRepository.findOne(projectId)).thenReturn(project);
        doNothing().when(projectRepository).flush();

        mvc.perform(
                post("/project/{project_id}/project-participant/add", projectId)
                .flashAttr("usersParticipants", usersParticipants)
            )
            .andExpect(redirectedUrl("/project/" + projectId + "/project_manager"))
            .andExpect(flash().attribute("statement","Success update Project participants"));

        verify(projectRepository, times(1)).findOne(projectId);
        verify(projectRepository, times(1)).flush();
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    public void cannotAssigneeProjectParticipantWhenFieldIsEmptyThrowException() throws Exception
    {
        Project project = new Project();

        List<User> users = new ArrayList<>();

        when(projectRepository.findOne(projectId)).thenReturn(project);
        when(bindingResult.hasErrors()).thenReturn(true);
        when(userRepository.findAll()).thenReturn(users);

        mvc.perform(
                post("/project/{project_id}/project-participant/add", projectId)
                .flashAttr("usersParticipants", new UsersParticipants())
            )
            .andExpect(status().is(422))
            .andExpect(model().hasErrors())
            .andExpect(model().attributeHasErrors("usersParticipants"))
            .andExpect(view().name("base"))
            .andExpect(model().attribute("id", projectId))
            .andExpect(model().attribute("view", "project/index"))
            .andExpect(model().attribute("project", project))
            .andExpect(model().attribute("AllUsers", users));

        verify(projectRepository, times(1)).findOne(projectId);
        verify(userRepository, times(1)).findAll();
        verifyNoMoreInteractions(projectRepository, userRepository);
    }
}
