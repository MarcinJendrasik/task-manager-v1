package pl.taskmanager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.taskmanager.controller.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskManagerApplicationTests {

	@Autowired
	BacklogController backlogController;

	@Autowired
	ProjectController projectController;

	@Autowired
	SprintController sprintController;

	@Autowired
	TaskCommentController taskCommentController;

	@Autowired
	TaskController taskController;

	@Autowired
	UserController userController;

	@Test
	public void contextLoads() {

		assertThat(backlogController).isNotNull();
		assertThat(projectController).isNotNull();
		assertThat(sprintController).isNotNull();
		assertThat(taskCommentController).isNotNull();
		assertThat(taskController).isNotNull();
		assertThat(userController).isNotNull();
	}

}
